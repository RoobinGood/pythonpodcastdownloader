import urllib3
import os, sys
import json
import re


class Logable:
	verbosity = True 

	def log(self, logData):
		if (self.verbosity):
			print("    log: ", logData)


class Podcast(Logable):
	channelTitle = ""
	title = ""

	def getTitle(self):
		return self.title

	def getName(self, index):
		return "_".join([self.getTitle(), str(index)])

	def makeRequest(self, url):
		poolManager = urllib3.PoolManager()
		r = poolManager.urlopen("GET", url)
		self.log("Status: {}".format(r.status))
		return r
		
	def prepareDownloadLink(self, index):
		pass

	def download(self, url, fileName):
		r = self.makeRequest(url)
		status = r.status
		if (status == 200):
			fileSize = int(r.getheader("Content-Length"))
			self.log("Length: {:.2f} MB".format(float(fileSize)/1024.0/1024.0))
			f = open(fileName, "wb")
			f.write(r.data)
			f.close()
			r.release_conn()
			return True
		else:
			self.log("Cannot download:" + r.status)
			return False

	def downloadByIndex(self, index, fileName):
		return self.download(self.prepareDownloadLink(index), fileName)

	def getChannelAddress(self):
		pass

	def getPodcastAddressByIndex(self, index):
		pass

	def getLastPodcastIndex(self):
		i = 100
		inc = 100
		if (self.verbosity):
			self.log("Trying urls...")
		while (True):
			if (self.makeRequest(self.getPodcastAddressByIndex(i)).status == 200):
				i = i + inc
			else:
				if (i <= 0):
					i = 1
				else:
					i = i - inc
					inc = int(inc/2)
			if (inc == 0):
				break
		self.log("Last index: {}".format(i))
		return i


class Podster(Podcast):
	downloadParameters = "download/audio.mp3?download=yes&media=file"
	channelAddressPattern = "http://***.podster.fm"

	def __init__(self, options):
		self.title = options["title"]
		self.channelTitle = options["channelTitle"]
		self.log("Init: " + self.title)

	def getChannelAddress(self):
		return self.channelAddressPattern.replace("***", self.channelTitle)  

	def getPodcastAddressByIndex(self, index):
		link = "/".join([self.getChannelAddress(), 
						str(index)])
		self.log(link)
		return link

	def prepareDownloadLink(self, index):
		link = "/".join([self.getPodcastAddressByIndex(index), 
						self.downloadParameters])
		self.log(link)
		return link


class Devzen(Podcast):
	channelAddressPattern = "http://devzen.ru/episode"

	def __init__(self, options):
		self.title = options["title"]
		self.log("Init: " + self.title)

	def getPodcastAddressByIndex(self, index):
		link = "-".join([self.channelAddressPattern, str(index).zfill(4)])
		self.log(link)
		return link

	def prepareDownloadLink(self, index):
		downloadLinkSectionPattern = '<a href="[\\w:\\/\\.-]*" class="powerpress_link_d" title="Скачать"'
		r = self.makeRequest(self.getPodcastAddressByIndex(index))
		html = r.data.decode("utf8")

		linkSection = re.search(downloadLinkSectionPattern, html).group()
		start = linkSection.find("\"") + 1
		end = linkSection.find("\"", start)
		link = linkSection[start:end]
		self.log(link)
		return link


class Podcaster(Logable):
	podcasts = []
	settings = {}

	def __init__(self, confFile):
		self.log("Init podcaster");
		self.settings = json.load(open(confFile))
		self.podcasts = [self.chooseModule(x["sourceModule"])(x) for x in self.settings["podcasts"]]

	def getPodcasts(self):
		return self.podcasts

	def chooseModule(self, name):
		if (name == "podster"):
			return Podster
		elif (name == "devzen"):
			return Devzen
		else:
			print("Unknown source module")

	def downloadConcretePodcast(self, podcastIndex, index, fileName):
		self.podcasts[podcastIndex].downloadByIndex(index, fileName)

	def findNewPodcasts(self):
		newPodcasts = {}
		for p in self.podcasts:
			newPodcasts[p.getTitle()] = p.getLastPodcastIndex()
		print(newPodcasts)


class Cli():
	podcaster = None

	def __init__(self, podcaster):
		self.podcaster = podcaster

	def showPodcast(self):
		podcasts = self.podcaster.getPodcasts()
		for i in range(0, len(podcasts)):
			print("{}. {}".format(i, podcasts[i].getTitle()))

	def choosePodcast(self):
		self.showPodcast()
		podcastIndex = int(input("Choose podcast:\n> "))
		return podcastIndex

	def run(self):
		print("Choose what do you want:")
		print("0 - exit")
		print("1 - findnew podcasts")
		print("2 - download concrete podcast")

		podcasts = self.podcaster.getPodcasts()
		command = -1
		while (command != "0"):
			command = input("> ")
			if (command == "0"):
				pass
			elif (command == "1"):
				self.podcaster.findNewPodcasts()
			elif (command == "2"):
				podcastIndex = self.choosePodcast()
				index = int(input("Choose index:\n> "))
				fileName = os.path.join(os.getcwd(), podcasts[podcastIndex].getName(index) + ".mp3")
				self.podcaster.downloadConcretePodcast(podcastIndex, index, fileName)
			else:
				print("Unknown command")


if __name__ == "__main__":
	podcaster = Podcaster("conf.json")
	cli = Cli(podcaster)
	cli.run()

	# index = podcasts[podcastIndex].getLastPodcastIndex()
	# podcasts[podcastIndex].downloadByIndex(index, fileName)

# todo:
# save/load options from file
# binary search of last index
# add another sources
# make dynamic modules load

# make module for options
# options:
# - title
# - channelTitle
# - sourceModuleTitle
# - lastIndex
# - 

# make cli with:
# - get all last podcasts
# - get concrete podcast
# - save to directory from options
# - 